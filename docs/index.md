# Test Page

This is a test page that is built through the Gitlab CI.
It makes use of the Drawio CLI.

![](./graph.drawio)


[Go to Source](https://gitlab.com/WaldemarLehner/drawio-mkdocs-headless-test/)
